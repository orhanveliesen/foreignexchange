package com.openpayd.ForeignExchange.services;

import com.openpayd.ForeignExchange.models.Conversion;
import com.openpayd.ForeignExchange.repositories.ConversionRepository;
import com.openpayd.ForeignExchange.resources.conversion.ConversionInput;
import com.openpayd.ForeignExchange.resources.conversion.ConversionOutput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateInput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateOutput;
import com.openpayd.ForeignExchange.services.exchangeService.ExchangeService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Just for example
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class MyConversionServiceTest {

    @MockBean
    ExchangeService exchangeService;

    @MockBean
    ConversionRepository conversionRepository;

    @Autowired
    MyConversionService conversionService;

    @Test
    void create() {
        ExchangeRateOutput exchangeOutput = new ExchangeRateOutput();
        exchangeOutput.setRate(1d);
        when(exchangeService.query(any(ExchangeRateInput.class))).thenReturn(exchangeOutput);
        when(conversionRepository.save(any(Conversion.class))).thenAnswer(invocationOnMock -> {
            Conversion conversion = invocationOnMock.getArgument(0);
            conversion.setId(1l);
            return conversion;
        });

        ConversionInput input = new ConversionInput();
        ZonedDateTime date = ZonedDateTime.now();
        input.setSourceAmunt(1d);
        input.setSourceCurrency(Currency.getInstance("USD"));
        input.setTargetCurrency(Currency.getInstance("USD"));
        ConversionOutput output = conversionService.create(input, date);
        assertEquals(output.getTargetAmount(), input.getSourceAmunt());
        assertEquals(output.getTargetCurrency(), input.getTargetCurrency());
        assertTrue(output.getTransactionId() != null);
        assertTrue(output.getTransactionId() > 0);
    }
}