package com.openpayd.ForeignExchange.repositories;

import com.openpayd.ForeignExchange.models.Conversion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface ConversionRepository extends PagingAndSortingRepository<Conversion, Long> {

    Page<Conversion> findAll(Specification<Conversion> spec, Pageable pageable);
}
