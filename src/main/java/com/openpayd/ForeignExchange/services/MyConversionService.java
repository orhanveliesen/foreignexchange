package com.openpayd.ForeignExchange.services;

import com.openpayd.ForeignExchange.models.Conversion;
import com.openpayd.ForeignExchange.repositories.ConversionRepository;
import com.openpayd.ForeignExchange.resources.conversion.ConversionInput;
import com.openpayd.ForeignExchange.resources.conversion.ConversionOutput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateInput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateOutput;
import com.openpayd.ForeignExchange.services.exchangeService.ExchangeService;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class MyConversionService {

    private ExchangeService service;

    private ConversionRepository conversionRepository;

    public MyConversionService(ExchangeService service, ConversionRepository conversionRepository) {
        this.service = service;
        this.conversionRepository = conversionRepository;
    }

    /**
     *
     * @param input
     * @param transactionDate this will increase the testability
     * @return
     */
    public ConversionOutput create(ConversionInput input, ZonedDateTime transactionDate) {
        ExchangeRateInput exchangeRateInput = new ExchangeRateInput();
        exchangeRateInput.setFrom(input.getSourceCurrency());
        exchangeRateInput.setTo(input.getTargetCurrency());
        ExchangeRateOutput response = service.query(exchangeRateInput);
        double ret = response.getRate() * input.getSourceAmunt();

        Conversion conversion = new Conversion();
        conversion.setSourceAmount(input.getSourceAmunt());
        conversion.setSourceCurrency(input.getSourceCurrency());
        conversion.setTargetAmount(ret);
        conversion.setTargetCurrency(input.getTargetCurrency());
        conversion.setTransactionDate(transactionDate);

        conversion = conversionRepository.save(conversion);

        ConversionOutput conversionOutput = new ConversionOutput();
        conversionOutput.setTargetAmount(ret);
        conversionOutput.setTargetCurrency(input.getTargetCurrency());
        conversionOutput.setTransactionId(conversion.getId());
        return conversionOutput;
    }

}
