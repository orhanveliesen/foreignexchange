package com.openpayd.ForeignExchange.services.exchangeService.ratesapi;

import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateInput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateOutput;
import com.openpayd.ForeignExchange.sdks.ratesapi.RateapiExchangeOutput;
import com.openpayd.ForeignExchange.sdks.ratesapi.RatesapiIOClient;
import com.openpayd.ForeignExchange.services.exchangeService.ExchangeService;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class RatesApiExchangeServiceAdaptor implements ExchangeService {

    private RatesapiIOClient client;

    private ExchangeOutputBuilder exchangeOutputBuilder = new ExchangeOutputBuilder();

    public RatesApiExchangeServiceAdaptor(RatesapiIOClient client) {
        this.client = client;
    }

    @Override
    public ExchangeRateOutput query(ExchangeRateInput input) {
        RateapiExchangeOutput output = client.query(input.getFrom().getCurrencyCode(), Arrays.asList(input.getTo().getCurrencyCode()));
        exchangeOutputBuilder.input(input);
        exchangeOutputBuilder.output(output);
        return exchangeOutputBuilder.build();
    }


    private static class ExchangeOutputBuilder {
        private ExchangeRateInput input;
        private RateapiExchangeOutput output;

        public void input(ExchangeRateInput input) {
            this.input = input;
        }

        public void output(RateapiExchangeOutput output) {
            this.output = output;
        }

        public ExchangeRateOutput build() {
            ExchangeRateOutput exchangeRateOutput = new ExchangeRateOutput();
            Double rate = output.getRates().get(input.getTo().getCurrencyCode());
            exchangeRateOutput.setRate(rate);

            return exchangeRateOutput;
        }
    }
}
