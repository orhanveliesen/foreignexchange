package com.openpayd.ForeignExchange.services.exchangeService;

import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateInput;
import com.openpayd.ForeignExchange.resources.exchange.ExchangeRateOutput;

public interface ExchangeService {
    ExchangeRateOutput query(ExchangeRateInput input) ;
}
