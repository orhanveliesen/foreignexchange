package com.openpayd.ForeignExchange.services;

import com.openpayd.ForeignExchange.models.Conversion;
import com.openpayd.ForeignExchange.repositories.ConversionRepository;
import com.openpayd.ForeignExchange.resources.conversionList.ConversionListInput;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class ConversionListService  {


    private ConversionRepository conversionRepository;
    private EntityManager entityManager;

    public ConversionListService(ConversionRepository conversionRepository, EntityManager entityManager) {
        this.conversionRepository = conversionRepository;
        this.entityManager = entityManager;
    }

    public Page<Conversion> query(ConversionListInput input, Pageable pageable) {

        Specification<Conversion> idSpecification = new Specification<Conversion>() {
            @Override
            public Predicate toPredicate(Root<Conversion> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("id"), input.getTransactionId());
            }
        };

        Specification<Conversion> transactionDateSpesification = new Specification<Conversion>() {
            @Override
            public Predicate toPredicate(Root<Conversion> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.greaterThanOrEqualTo(root.get("transactionDate"), input.getTransactionDate());
            }
        };

        return conversionRepository.findAll(idSpecification.or(transactionDateSpesification), pageable);
    }
}
