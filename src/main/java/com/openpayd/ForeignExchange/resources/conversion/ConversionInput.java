package com.openpayd.ForeignExchange.resources.conversion;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Currency;

public class ConversionInput {
    @NotNull
    @DecimalMin("0.0")
    private Double sourceAmunt;
    @NotNull
    private Currency sourceCurrency;
    @NotNull
    private Currency targetCurrency;
    private ZonedDateTime transactionDate;

    public Double getSourceAmunt() {
        return sourceAmunt;
    }

    public void setSourceAmunt(Double sourceAmunt) {
        this.sourceAmunt = sourceAmunt;
    }

    public Currency getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(Currency sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public Currency getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(Currency targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }
}
