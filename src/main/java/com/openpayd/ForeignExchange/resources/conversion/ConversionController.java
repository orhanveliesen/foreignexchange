package com.openpayd.ForeignExchange.resources.conversion;

import com.openpayd.ForeignExchange.services.MyConversionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.ZonedDateTime;

@RestController
@RequestMapping("conversion")
public class ConversionController {

    private MyConversionService myConversionService;

    public ConversionController(MyConversionService myConversionService) {
        this.myConversionService = myConversionService;
    }

    /**
     *
     * @param input ConversionInput
     * @return ConversionOutput
     */
    @PostMapping
    public ConversionOutput create(@Valid @RequestBody ConversionInput input){
        return myConversionService.create(input, ZonedDateTime.now());
    }
}
