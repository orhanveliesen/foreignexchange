package com.openpayd.ForeignExchange.resources.conversionList;

import com.openpayd.ForeignExchange.resources.conversionList.validators.annotations.CheckAtLeastOneNotNull;

import java.time.ZonedDateTime;


@CheckAtLeastOneNotNull(fieldNames={"transactionId","transactionDate"}, message = "At least one field (transactionId or transactionDate) must not be null ")
public class ConversionListInput {

    private Long transactionId;
    private ZonedDateTime transactionDate;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }
}
