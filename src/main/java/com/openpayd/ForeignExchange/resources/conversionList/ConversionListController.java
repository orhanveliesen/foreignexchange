package com.openpayd.ForeignExchange.resources.conversionList;

import com.openpayd.ForeignExchange.models.Conversion;
import com.openpayd.ForeignExchange.services.ConversionListService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("conversionList")
public class ConversionListController {

    private ConversionListService conversionListService;

    public ConversionListController(ConversionListService conversionListService) {
        this.conversionListService = conversionListService;
    }


    /**
     * unfortunately Pageable does not work with spring fox 2. This is a known issue. So i did this implementation explicitly
     *
     * @param input ConversionListInput
     * @param page  int
     * @param size  int
     * @return Page<Conversion>
     */
    @PostMapping
    public ResponseEntity<Page<Conversion>> query(
            @RequestBody @Valid ConversionListInput input,
            @RequestParam(value = "page", defaultValue = "0") Integer page, @RequestParam(value = "size", defaultValue = "20") Integer size) {
        return ResponseEntity.ok(conversionListService.query(input, PageRequest.of(page, size)));
    }

}
