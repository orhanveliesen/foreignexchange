package com.openpayd.ForeignExchange.resources.conversionList;

import com.openpayd.ForeignExchange.models.Conversion;
import org.springframework.data.domain.Page;

public class ConversionListOutput  {

    private Page<Conversion> page = Page.empty();

    public ConversionListOutput(Page<Conversion> page) {
        this.page = page;
    }

    public ConversionListOutput(Conversion conversion) {
        page.getContent().add(conversion);
    }

    public Page<Conversion> getPage() {
        return page;
    }

    public void setPage(Page<Conversion> page) {
        this.page = page;
    }
}
