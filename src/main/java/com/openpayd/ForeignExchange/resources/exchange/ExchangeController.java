package com.openpayd.ForeignExchange.resources.exchange;

import com.openpayd.ForeignExchange.services.exchangeService.ExchangeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("exchange")
public class ExchangeController {

    private ExchangeService exchangeService;

    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @PostMapping
    public ExchangeRateOutput query(@RequestBody  ExchangeRateInput input){
        return exchangeService.query(input);
    }

}
