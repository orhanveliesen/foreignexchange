package com.openpayd.ForeignExchange.resources.exchange;

import java.util.Currency;

public class ExchangeRateInput {
    private Currency from;
    private Currency to;

    public Currency getFrom() {
        return from;
    }

    public void setFrom(Currency from) {
        this.from = from;
    }

    public Currency getTo() {
        return to;
    }

    public void setTo(Currency to) {
        this.to = to;
    }
}
