package com.openpayd.ForeignExchange.resources.exchange;

public class ExchangeRateOutput {
    private Double rate;

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getRate() {
        return rate;
    }
}
