package com.openpayd.ForeignExchange.resources.errorHandling;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
public class ErrorHandling  {
    private static final String UNEXPECTED_ERROR = "Exception.unexpected";
    private final MessageSource messageSource;

    public ErrorHandling(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(RestException.class)
    public ResponseEntity<Object> handleIllegalArgument(RestException ex, Locale locale) {
        String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleArgumentNotValidException(MethodArgumentNotValidException ex, Locale locale) {
        BindingResult result = ex.getBindingResult();
        List<String> errorMessages = result.getAllErrors()
                .stream()
                .map(
                        objectError ->{

                            return buildError(objectError, locale);
                        })
                .collect(Collectors.toList());

        return new ResponseEntity<>(new RestMessage(errorMessages), HttpStatus.BAD_REQUEST);
    }

    private String buildFieldError(FieldError objectError, Locale locale){
        String localizedErrorMsg = "";
        String errorCode = objectError.getCode();
        try {
            localizedErrorMsg = messageSource.getMessage(errorCode.toLowerCase(), new Object[]{objectError.getField()}, locale);
        } catch (Exception ex2) {
            try {
                localizedErrorMsg = messageSource.getMessage(objectError.getDefaultMessage().toString(), new Object[]{objectError.getField()}, locale);
            } catch (Exception ex3) {
                localizedErrorMsg = objectError.getDefaultMessage();
            }
        }
        return localizedErrorMsg;
    }

    private String buildObjectError(ObjectError objectError, Locale locale){
        String localizedErrorMsg = "";
        String errorCode = objectError.getCode();
        try {
            localizedErrorMsg = messageSource.getMessage(errorCode.toLowerCase(), objectError.getArguments(), locale);
        } catch (Exception ex2) {
            try {
                localizedErrorMsg = messageSource.getMessage(objectError.getDefaultMessage().toString(), objectError.getArguments(), locale);
            } catch (Exception ex3) {
                localizedErrorMsg = objectError.getDefaultMessage();
            }
        }
        return localizedErrorMsg;
    }

    private String buildError(ObjectError error, Locale locale){
        if (error instanceof FieldError) return buildFieldError((FieldError) error, locale);
        return buildObjectError(error,locale);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestMessage> handleExceptions(Exception ex, Locale locale) {
        String errorMessage = messageSource.getMessage(UNEXPECTED_ERROR, null, locale);
        ex.printStackTrace();
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<RestMessage> handleExceptions(HttpMessageNotReadableException ex, Locale locale) {
        String errorMessage = messageSource.getMessage(ex.getLocalizedMessage(), null, locale);
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
    }

}
