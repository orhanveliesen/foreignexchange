package com.openpayd.ForeignExchange.resources.errorHandling;

public class RestException extends RuntimeException {
    private String message;

    private Object[] args;

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RestException(String message) {
        super(message);
    }
}
