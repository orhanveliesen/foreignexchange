package com.openpayd.ForeignExchange.sdks;

import com.openpayd.ForeignExchange.sdks.ratesapi.RateapiExchangeOutput;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ExchangeClient {
    RateapiExchangeOutput query(@RequestParam("base") String base, @RequestParam("symbols") List<String> symbols);

}
