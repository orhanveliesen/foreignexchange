package com.openpayd.ForeignExchange.sdks.ratesapi;

import com.openpayd.ForeignExchange.config.FeignClientConfig;
import com.openpayd.ForeignExchange.sdks.ExchangeClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "ratesapiio", url = "https://api.ratesapi.io", configuration = FeignClientConfig.class)
public interface RatesapiIOClient extends ExchangeClient {

    @RequestMapping(method = RequestMethod.GET, value="/api/latest", produces = "application/json")
    RateapiExchangeOutput query(@RequestParam("base") String base, @RequestParam("symbols") List<String> symbols);


}
