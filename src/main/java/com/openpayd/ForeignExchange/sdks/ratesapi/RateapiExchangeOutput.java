package com.openpayd.ForeignExchange.sdks.ratesapi;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {"base":"USD","rates":{"GBP":0.8014766578},"date":"2020-04-15"}
 */
public class RateapiExchangeOutput {

    private String base;
    private Map<String, Double> rates = new HashMap<>();
    private Date date;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
